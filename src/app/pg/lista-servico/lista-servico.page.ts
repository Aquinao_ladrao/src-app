import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http';
import {UrlService} from '../../servidor/url.service';
import {map} from 'rxjs/operators';
@Component({
  selector: 'app-lista-servico',
  templateUrl: './lista-servico.page.html',
  styleUrls: ['./lista-servico.page.scss'],
})
export class ListaServicoPage implements OnInit {
  servicos: any;
  constructor(public http: Http, public servidorUrl: UrlService) {
    this.listaServicos();
   }
  listaServicos(){
    this.http.get(this.servidorUrl.pegarUrl()+'lista-servico.php').pipe(map(res =>res.json()))
    .subscribe(
      listaDados =>{
        this.servicos=listaDados;
        console.log(this.servicos);
      });
      
     
      
  }
  

  ngOnInit() {}

}
//fim da classe (não escreva nada abixo disso (: )
