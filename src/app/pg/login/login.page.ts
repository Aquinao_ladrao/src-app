import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { UrlService } from '../../servidor/url.service';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { UsuarioService } from 'src/app/servidor/usuario.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  email: string;
  senha: string;

  constructor(
    public http: Http,
    public servidorUrl: UrlService,
    public nav: NavController,
    public dadosUser: UsuarioService,
    public alerta: AlertController,
    public load: LoadingController
  ) {
    
  } // FIM CONSTRUCTOR

  ngOnInit() {
    document.querySelector('ion-tab-bar').style.display = 'none';
  }

  async logar() {
    if (this.email == undefined || this.senha == undefined) {
      this.servidorUrl.alertas('Atenção', 'Preencha os campos');
    } else {
      const loading = await this.load.create({
        spinner: 'circles',
        message: 'Verificando login...'
      });
      await loading.present();

      this.http
        .get(this.servidorUrl.pegarUrl() + 'login.php?email=' + this.email + '&senha=' + this.senha)
        .pipe(map(res => res.json()))
        .subscribe(data => {
          if (data.msg.logado == 'sim') {
            
            loading.dismiss();

            /*Alimentar as variaveis */
            this.dadosUser.setIdusuario(data.Dados.codigo);
            this.dadosUser.setemailUsuario(data.Dados.email);
            this.dadosUser.setnomeUsuario(data.Dados.nome);
            
            this.nav.navigateBack('/tabs/pg/home');
          } else {
            loading.dismiss();
            this.servidorUrl.alertas('Atenção', 'Usuário ou Senha incorreto');
          }
        });
    }
  }
} // FIM DA CLASS
