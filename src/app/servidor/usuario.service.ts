import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  idUsuario = '';
  nomeUsuario = '';
  emailUsuario = '';
  fotoUsuario = '';

  constructor() { }

  setIdusuario(valor){
    this.idUsuario = valor;

  }

  getidUsuario(){
    return this.idUsuario;
  }

  setnomeUsuario(valor){
    this.nomeUsuario = valor;

  }

  getnomeUsuario(){
    return this.nomeUsuario;
  }

  setemailUsuario(valor){
    this.emailUsuario = valor;

  }

  getemailUsuario(){
    return this.emailUsuario;
  }

  setfotoUsuario(valor){
    this.fotoUsuario = valor;

  }

  getfotoUsuario(){
    return this.fotoUsuario;
  }

}
