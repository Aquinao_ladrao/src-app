import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'home', loadChildren: './pg/home/home.module#HomePageModule' },
  { path: 'sobre', loadChildren: './pg/sobre/sobre.module#SobrePageModule' },
  { path: 'lista-servico', loadChildren: './pg/lista-servico/lista-servico.module#ListaServicoPageModule' },
  { path: 'servico', loadChildren: './pg/servico/servico.module#ServicoPageModule' },
  { path: 'perfil', loadChildren: './pg/perfil/perfil.module#PerfilPageModule' },
  { path: 'login', loadChildren: './pg/login/login.module#LoginPageModule' },
  { path: 'cadastro', loadChildren: './pg/cadastro/cadastro.module#CadastroPageModule' },
  { path: 'reserva', loadChildren: './pg/reserva/reserva.module#ReservaPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
