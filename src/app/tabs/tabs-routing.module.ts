import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs/pg',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () => import('../pg/home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'sobre',
        children: [
          {
            path: '',
            loadChildren: () => import('../pg/sobre/sobre.module').then(m => m.SobrePageModule)
          }
        ]
      },
      {
        path: 'lista-servico',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pg/lista-servico/lista-servico.module').then(m => m.ListaServicoPageModule)
          }
        ]
      },
      {
        path: 'login',
        children: [
          {
            path: '',
            loadChildren: () => import('../pg/login/login.module').then(m => m.LoginPageModule)
          }
        ]
      },
      {
        path: 'perfil',
        children: [
          {
            path: '',
            loadChildren: () => import('../pg/perfil/perfil.module').then(m => m.PerfilPageModule)
          }
        ]
      },
      {
        path: 'Cadastro',
        children: [
          {
            path: '',
            loadChildren: () => import('../pg/cadastro/cadastro.module').then(m => m.CadastroPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/pg/home',
        pathMatch: 'full'
      }
    ]
  },

  {
    path: '',
    redirectTo: '/tabs/pg/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
